# Kubernetes-Exercise

# Project Name

Deply Redis.

# Objectives

	1. Deploy redis with security enabled
	2. Add a config map which sets redis logging to debug mode
	
# Creating the work envirement

* Create Gitlab account
* Install minikube

# Deploy redis with security enabled 

1. Create a secret file:

    Create a password Based 64 and set it in "password" field:

```
echo -n 'MyPass' | base64
UnIxMjMjIyM=
```

(I Used another password)

 
2. Apply the secret file:
```
$ kubectl create -f redis-secret.yaml
```
[redis-secret.yaml](https://gitlab.com/Roeywer/kubernetes-exercise/-/blob/main/redis-secret.yaml)

3. Create and apply Configmap file for the second objective:

```
$ kubectl create -f redis-configmap.yaml
 ```

[redis-configmap.yaml](https://gitlab.com/Roeywer/kubernetes-exercise/-/blob/main/redis-configmap.yaml)


3. Create .yaml file to deply radis Pod:

   [redis-pod.yaml](https://gitlab.com/Roeywer/kubernetes-exercise/-/blob/main/redis-pod.yaml)

   [I used redis template from this url](https://kubernetes.io/docs/tutorials/configuration/configure-redis-using-configmap/)

   I Set the version to the latest, 6.2, add the password section with "--requirepass". without this argumment you be able to login without a      password.

   To create the POD run:
```
$ kubectl create -f redis-pod.yaml
```
4. Test login to Redis Server with a password:

    Run the command:
```
$ kubectl exec -it redis -- redis-cli to get the shell:
 ```

    127.0.0.1:6379>

    I tried to run the command "info" and got an error "(error) WRONGPASS invalid username-password pair or user is disabled."

    To autenticate type AUTH and the password.
    the output should be OK.

5. Update the Password:

    To update the password you can update the secret file "password:" field as mention in section 1.

# Add a config map which sets redis logging to debug mode

 1. I update the end of the yaml file key with the "configMap:" Section to call "redis-configmap.yaml"
 
 2. added "LOG_LEVEL: debug" to configmap to set the pod log level to debug. 
    

  










